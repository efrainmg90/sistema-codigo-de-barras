namespace Inventario.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Log")]
    public partial class Log
    {
        public int Id { get; set; }

        public int Product_id { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime Datetime { get; set; }

        [Required]
        [StringLength(7)]
        public string Type { get; set; }

        public long? Serial { get; set; }

        public virtual Product Product { get; set; }
    }
}
