namespace Inventario.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Product")]
    public partial class Product
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Product()
        {
            Log = new HashSet<Log>();
        }

        public int Id { get; set; }

        [Display(Name = "Producto")]
        public int Caproduct_id { get; set; }

        [Display(Name = "Pedido")]
        [Required]
        public  int Order_id { get; set; }

        [Display(Name = "Largo")]
        public float Long { get; set; }

        [Display(Name = "Ancho")]
        public float Width { get; set; }

        [Required]
        [StringLength(15)]
        public string Color { get; set; }

        public long Barcode { get; set; }

        [Display(Name = "Cantidad")]
        public int Quantity { get; set; }

       
        public virtual Order Order { get; set; }

        public virtual CaProduct CaProduct { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        [JsonIgnore]
        public virtual ICollection<Log> Log { get; set; }
    }
}
