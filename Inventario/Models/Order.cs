﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventario.Models
{
    [Table("Order")]
    public partial class Order
    {
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string Folio { get; set; }

        [Required]
        [Display(Name = "Cliente")]
        public int Customer_id { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Destino")]
        public string Place { get; set; }

        public virtual Customer Customer { get; set; }

        [JsonIgnore]
        public virtual ICollection<Product> Product { get; set; }
    }
}