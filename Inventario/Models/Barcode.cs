namespace Inventario.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Microsoft.AspNet.Identity.EntityFramework;

    public partial class BarcodeDBEntities : IdentityDbContext<ApplicationUser>
    {
        public BarcodeDBEntities()
            : base("name=Barcode", throwIfV1Schema:false)
        {
        }

        public static BarcodeDBEntities Create()
        {
            return new BarcodeDBEntities();
        }

        //public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
       // public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
       // public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
       // public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<CaProduct> CaProduct { get; set; }
        public virtual DbSet<Color> Color { get; set; }
        public virtual DbSet<Log> Log { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Order> Order { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            /*
            modelBuilder.Entity<AspNetRoles>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUsers>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUsers)
                .HasForeignKey(e => e.UserId);*/

        modelBuilder.Entity<CaProduct>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<CaProduct>()
                .Property(e => e.Code)
                .IsUnicode(false);
         

            modelBuilder.Entity<CaProduct>()
                .HasMany(e => e.Product)
                .WithRequired(e => e.CaProduct)
                .HasForeignKey(e => e.Caproduct_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
               .HasMany(e => e.Product)
               .WithRequired(e => e.Order)
               .HasForeignKey(e => e.Order_id)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Color>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Log>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<Product>()
                .Property(e => e.Color)
                .IsUnicode(false);

  
            modelBuilder.Entity<Product>()
                .HasMany(e => e.Log)
                .WithRequired(e => e.Product)
                .HasForeignKey(e => e.Product_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Customer>()
               .HasMany(e => e.Order)
               .WithRequired(e => e.Customer)
               .HasForeignKey(e => e.Customer_id)
               .WillCascadeOnDelete(false);


            base.OnModelCreating(modelBuilder);
            // modelBuilder.Entity<Users>()
            //     .MapToStoredProcedures(s => s.Delete(u => u.HasName("DeleteUserById", "dbo")
            //                                   .Parameter(b => b.id, "userid"))
            //);
        }

    }
}
