﻿using Inventario.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventario.ViewModels
{
    public class OrderFormViewModel
    {
        public IEnumerable<Color> Colors { get; set; }
        public IEnumerable<CaProduct> CaProducts { get; set; }
        public IEnumerable<Customer> Customers { get; set; }
        public Product Product { get; set; }
    }
}