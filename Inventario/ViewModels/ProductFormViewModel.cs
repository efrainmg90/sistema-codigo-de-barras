﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Inventario.Models;

namespace Inventario.ViewModels
{
    public class ProductFormViewModel
    {
        public IEnumerable<Color> Colors { get; set; }
        public IEnumerable<CaProduct> CaProducts { get; set; }
        public Product Product { get; set; }
    }
}