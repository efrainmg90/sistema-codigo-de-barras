﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace Inventario.Helper
{
    public class Label
    {
        BarcodeLib.Barcode barcode;
        public Label()
        {
            barcode = new BarcodeLib.Barcode();
        }

        public void create(string code)
        {
            barcode.IncludeLabel = true;
            Image img = barcode.Encode(BarcodeLib.TYPE.CODE128, code, Color.Black, Color.White, 300, 150);
            barcode.SaveImage(HttpContext.Current.Server.MapPath("~/Content/labels/" + code + ".jpg"), BarcodeLib.SaveTypes.JPG);
        }

    }
}