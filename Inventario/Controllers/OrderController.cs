﻿using Inventario.Helper;
using Inventario.Models;
using Inventario.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inventario.Controllers
{
   
    public class OrderController : Controller
    {
        private BarcodeDBEntities _context;
        private Label label;
        public OrderController()
        {
            _context = new BarcodeDBEntities();
            label = new Label();
        }
        // GET: Order
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult New()
        {
            var viewModel = new OrderFormViewModel {
                Customers = _context.Customer.ToList(),
                Colors = _context.Color.ToList(),
                CaProducts = _context.CaProduct.ToList(),
                Product = new Product { Id = 0, Quantity = 0 }
            };
            return View(viewModel);
        }

        public ActionResult Save(ICollection<Product> products, Order order)
        {
            int val = 11;
            string res="", msg="";
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    Order orderdb = _context.Order.Add(order);
                    _context.SaveChanges();
                   // transaction.Commit();
                    foreach (var product in products)
                    {
                        product.Order_id = orderdb.Id;
                        string str = DateTime.Now.ToString("yyyyMMddHHmmss");
                        str = str + (val);
                        product.Barcode = Convert.ToInt64(str);
                        _context.Product.Add(product);
                        _context.SaveChanges();
                        label.create(product.Barcode.ToString());
                        val++;
                    }
                    transaction.Commit();
                    res = "success"; msg="Pedido Guardado";
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                    transaction.Rollback();
                    res = "error";msg = "Error al guardar";
                }
            }
            return Json(new {res=res,msg=msg });
        }
    }
}