﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inventario.Models;
using Inventario.ViewModels;
using Inventario.Helper;
using System.Net;
using System.IO;
using System.Data.Entity;
using System.Data;
using ClosedXML.Excel;
using BarcodeLib;
using System.Drawing;

namespace Inventario.Controllers
{
   
    public class ProductController : Controller
    {
        private BarcodeDBEntities _context;
        private Label label;
        public ProductController()
        {
            _context = new BarcodeDBEntities();
            label = new Label();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        [Authorize(Roles = "Admin, User")]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize(Roles = "Admin, User")]
        public ActionResult Report()
        {
            var products = _context.Product.Include(c => c.CaProduct).Include(c => c.Order)
               .ToList();
            DataTable dt = new DataTable("Product");
            dt.Columns.AddRange(new DataColumn[10] { new DataColumn("Folio"),
                                            new DataColumn("Cliente"),
                                            new DataColumn("Producto"),
                                            new DataColumn("Color"),
                                            new DataColumn("Ancho"),
                                            new DataColumn("Largo"),
                                            new DataColumn("Material en piso"),
                                            new DataColumn("Metros Cuadrados"),
                                            new DataColumn("Peso"),
                                            new DataColumn("Destino")
            });
            dt.Columns[7].DataType = typeof(Double);
            dt.Columns[6].DataType = typeof(Double);
            dt.Columns[5].DataType = typeof(Int64);
            foreach (var product in products)
            {
                if (product.Quantity < 1)
                    continue;
                var m2 = product.Quantity * product.Long * product.Width;
                dt.Rows.Add(product.Order.Folio,product.Order.Customer.Name ,product.CaProduct.Name, product.Color, product.Width,
                            product.Long, product.Quantity, m2, m2*product.CaProduct.Weight,product.Order.Place);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return base.File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Material en piso "+DateTime.Now+".xlsx");
                }
            }
            //return RedirectToAction("Index", "Product"); 

        }

        [Authorize(Roles = "Admin, User")]
        public ActionResult Log()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Reportlog()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin, User")]
        public ActionResult ReportLogPost(DateTime dateinitial, DateTime datefinal)
        {
           datefinal= datefinal.AddHours(23);
           datefinal= datefinal.AddMinutes(59);
            var logs = _context.Log.Include(l => l.Product).Include(l => l.Product.CaProduct).
                Where(l=>l.Datetime >= dateinitial && l.Datetime <= datefinal).ToList();

            DataTable dt = new DataTable("Log");
            dt.Columns.AddRange(new DataColumn[7] { 
                                            new DataColumn("Producto"),
                                            new DataColumn("Color"),
                                            new DataColumn("Ancho(M)"),
                                            new DataColumn("Largo(M)"),
                                            new DataColumn("Pedido"),
                                            new DataColumn("Fecha"),
                                            new DataColumn("Tipo")
            });

            foreach (var log in logs)
            {
                dt.Rows.Add( log.Product.CaProduct.Name, log.Product.Color, log.Product.Width,
                            log.Product.Long, log.Product.Order.Folio, log.Datetime.ToString("yyyy-MM-dd"), log.Type);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return base.File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Logs " + DateTime.Now + ".xlsx");
                }
            }
            //return RedirectToAction("Index", "Product"); 
        }

        [Authorize(Roles = "Admin")]
        public ActionResult New()
        {
            //long barcode = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmss"));
            var viewModel = new ProductFormViewModel
            {
                CaProducts = _context.CaProduct.OrderBy(p=>p.Name).ToList(),
                Colors = _context.Color.OrderBy(c=>c.Name).ToList(),
                Product = new Product { Id = 0, Quantity=0 }
            };
            return View(viewModel);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create(Product product)
        {
            if (product.Id == 0)
            {
                product.Barcode = Convert.ToInt64(DateTime.Now.ToString("yyyyMMddHHmmss"));
                _context.Product.Add(product);
                _context.SaveChanges();
                string code = Convert.ToString(product.Barcode);
                label.create(code);
                //webClient.DownloadFile(Server.MapPath("~/labels/" + code + ".jpg"), code+".jpg");
                //webClient.DownloadFile("http://localhost:50147/Product/Index",Server.MapPath("~/labels/" + code + ".jpg"));
            }
            return RedirectToAction("Index","Product");
        }

        [Authorize(Roles = "Admin, User")]
        public ActionResult Label(string id)
        {
            
            var dir = Server.MapPath(" ~/Content/labels");
            var path = Path.Combine(dir, id + ".jpg"); //validate the path for security or use other means to generate the path.
            return base.File(path, "image/jpeg");
        }

        public void saveLabel(string code)
        {
            Barcode barcode = new BarcodeLib.Barcode();
            barcode.IncludeLabel = true;
            Image img = barcode.Encode(BarcodeLib.TYPE.CODE128, code, System.Drawing.Color.Black, System.Drawing.Color.White, 300, 150);
            var dir = Server.MapPath(" ~/Content/labels");
            string path = Path.Combine(dir, code,  ".jpg");
            barcode.SaveImage(path, BarcodeLib.SaveTypes.JPG);
            //barcode.SaveImage(HttpContext.Current.Server.MapPath("~/Content/labels/" + code + ".jpg"), BarcodeLib.SaveTypes.JPG);
            //barcode.SaveImage(HttpContext.Current.Server.MapPath("~/Content/labels/" + code + ".jpg"), BarcodeLib.SaveTypes.JPG);

            //barcode.SaveImage(Url.c), BarcodeLib.SaveTypes.JPG);

        }
    }
}