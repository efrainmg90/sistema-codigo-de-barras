﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Inventario.Models;
using System.Data.Entity;
using Inventario.Models.DTO;

namespace Inventario.Controllers.Api
{
    public class LogController : ApiController
    {
        private BarcodeDBEntities _context;

        public LogController()
        {
            _context = new BarcodeDBEntities();
        }

        public IHttpActionResult GetLogs()
        {
            var logs = _context.Log.Include(l => l.Product).Include(l => l.Product.CaProduct).Include(l => l.Product.Order).ToList();
            return Ok(logs);
        }

        public IHttpActionResult CreateLogs(LogBarcodeDto logBarcode)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            var product = _context.Product.FirstOrDefault(p => p.Barcode == logBarcode.Barcode);
            if (product == null)
                return NotFound();
            var log = new Log {
                Product_id = product.Id,
                Datetime = DateTime.Now,
                Type = logBarcode.Type
            };
            _context.Log.Add(log);
            _context.SaveChanges();
            return Ok("success");
            //return Conflict(); para bajas en 0
        }
    }
}
