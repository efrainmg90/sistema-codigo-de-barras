﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Inventario.Models;

namespace Inventario.Controllers.Api
{
    public class ProductController : ApiController
    {
        private BarcodeDBEntities _context;

        public ProductController()
        {
            _context = new BarcodeDBEntities();
        }

        // GET /api/products
        public IHttpActionResult GetProducts()
        {
            var products = _context.Product.Include(c=>c.CaProduct).Include(c=>c.Order)
                .ToList();
            return Ok(products);
        }
    }
}
