﻿using Lector.DTO;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


namespace Lector
{
    class CallApi
    {
        //HttpClient client;

        public CallApi()
        {
           // client = new HttpClient();
        }

        public async Task<string> CreateLog(LogBarcodeDto logBarcode)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://192.168.3.116:81");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.PostAsJsonAsync("api/log", logBarcode);
                //response.EnsureSuccessStatusCode();
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return "Created";

                }
                else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return "Not Found";
                else if (response.StatusCode == System.Net.HttpStatusCode.Conflict)
                    return "Zero";
                else
                    return "error";
            }
        }
    }
}
