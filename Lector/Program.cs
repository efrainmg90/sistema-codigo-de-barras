﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreScanner;
using System.ServiceProcess;

namespace Lector
{
    class Program
    {
        //static CCoreScanner cCoreScannerClass;
        static void Main(string[] args)
        {

#if DEBUG
            //While debugging this section is used.
            ServicioScaneo myService = new ServicioScaneo();
            myService.onDebug();
            System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);

#else
    //In Release this section is used. This is the "normal" way.
    ServiceBase[] ServicesToRun;
    ServicesToRun = new ServiceBase[] 
    { 
        new ServicioScaneo()
    };
    ServiceBase.Run(ServicesToRun);
#endif

        }
    }
}
