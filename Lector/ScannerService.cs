﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Lector
{
    partial class ServicioScaneo : ServiceBase
    {
        ScannerApi api;
       
        public ServicioScaneo()
        {
            api = new ScannerApi();
            InitializeComponent();
            
        }

        protected override void OnStart(string[] args)
        {
            // TODO: agregar código aquí para iniciar el servicio.
            api.Open();
            api.SuscribeEventsScanner();
          
        }

        protected override void OnStop()
        {
            // TODO: agregar código aquí para realizar cualquier anulación necesaria para detener el servicio.
        }

        public void onDebug()
        {
            OnStart(null);
        }
    }
}
