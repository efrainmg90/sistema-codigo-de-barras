﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreScanner;
using System.Xml;
using Lector.DTO;

namespace Lector
{
    class ScannerApi
    {
        CCoreScanner cCoreScannerClass;
        FileConfig fileConfig;
        Dictionary<string, string[]> scannersDict;//variable propiedades de scanners serial->propiedades
        CallApi callApi;
        short[] scannerTypes = new short[1];//Scanner Types you are interested in
        short numberOfScannerTypes = 1; // Size of the scannerTypes array
        int status; // Extended API return code

        public ScannerApi()
        {
            cCoreScannerClass = new CCoreScanner();
            scannerTypes[0] = 1; // 1 for all scanner types
            fileConfig = new FileConfig();
            callApi = new CallApi();
        }

        public void Open()
        {
            try
            {
                fileConfig.LoadConfigurationServer();
                scannersDict = fileConfig.LoadConfigurationScanners();
                cCoreScannerClass.Open(0, scannerTypes, numberOfScannerTypes, out status);
                cCoreScannerClass.BarcodeEvent += new _ICoreScannerEvents_BarcodeEventEventHandler(OnBarcodeEvent);
            }
            catch (Exception)
            {

                throw;
            }

        }
        public void SuscribeEventsScanner() {
            int opcode = 1001; // Method for Subscribe events
            string outXML; // XML Output
            string inXML = "<inArgs>" +
            "<cmdArgs>" +
            "<arg-int>1</arg-int>" + // Number of events you want to subscribe
            "<arg-int>1</arg-int>" + // Comma separated event IDs
            "</cmdArgs>" +
            "</inArgs>";
            cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out status);
            //Console.WriteLine(outXML);
            //Console.ReadKey();
        }

        void OnBarcodeEvent(short eventType, ref string pscanData)
        {
            string barcode = pscanData;
            Console.WriteLine(barcode);
            Dictionary<string, string> dict = ShowBarcodeLabel(barcode);
            //-0
            if (scannersDict.ContainsKey("")) { }
            string[] values = scannersDict[dict["serial"]];
            Console.WriteLine("CODIGO: "+dict["barcode"]);
            Console.WriteLine("SERIAL: " + values[0]);
            Console.WriteLine("TIPO: " + values[2]);
            try
            {
                var logBarcode = new LogBarcodeDto
                {
                    Barcode = Convert.ToInt64(dict["barcode"]),
                    Serial = Convert.ToInt64(dict["serial"]),
                    Type = values[2] == "IN" ? "ENTRADA" : "SALIDA"
                };

                var task = callApi.CreateLog(logBarcode);
                task.Wait();
                string result = task.Result;
                if(result== "Created")
                    BeepSuccessScanner(values[1]);
                else
                    BeepErrorScanner(values[1], "1");
                Console.WriteLine(result);
            }
            catch (Exception e)
            {
                BeepErrorScanner(values[1],"1");
                Console.WriteLine("Error de codigo");
            }
            
            //var url = await callApi.CreateLog(logBarcode);
        }

        public Dictionary<string,string> ShowBarcodeLabel(string strXml)
        {
            var dict = new Dictionary<string, string>();
            System.Diagnostics.Debug.WriteLine("Initial XML" + strXml);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(strXml);

            string strData = String.Empty;
            string barcode = xmlDoc.DocumentElement.GetElementsByTagName("datalabel").Item(0).InnerText;
            string serial = xmlDoc.DocumentElement.GetElementsByTagName("serialnumber").Item(0).InnerText;
            //string symbology = xmlDoc.DocumentElement.GetElementsByTagName("datatype").Item(0).InnerText;
            string[] numbers = barcode.Split(' ');

            foreach (string number in numbers)
            {
                if (String.IsNullOrEmpty(number))
                {
                    break;
                }

                strData += ((char)Convert.ToInt32(number, 16)).ToString();
            }
            dict.Add("barcode", strData.Trim());
            dict.Add("serial", serial.Trim());
            return dict;
        }//end showbarcodelabel

        void BeepSuccessScanner(string serialNumberScanner)
        {
            string[] actions = new string[] { "3", "43", "42", "43", "42" };
            string inXml = null;
            int opCode = 6000;
            string outXml = "";
            int status_beep = 1;
            string ScannerId = this.GetScannerID(serialNumberScanner);
            foreach (string action in actions)
            {
                inXml = "<inArgs>" +
                                       "<scannerID>" + ScannerId + "</scannerID>" +
                                       "<cmdArgs>" +
                                       "<arg-int>" + action
                                       + "</arg-int>" +
                                       "</cmdArgs>" +
                                       "</inArgs>";

                cCoreScannerClass.ExecCommand(opCode, ref inXml, out outXml, out status_beep);
            }
        }

        protected string GetScannerID(string serialNumberScanner)
        {
            short numberOfScanners; // Number of scanners expect to be used
            int[] connectedScannerIDList = new int[255];
            // List of scanner IDs to be returned
            string outXML; //Scanner details output
          
            cCoreScannerClass.GetScanners(out numberOfScanners, connectedScannerIDList,
            out outXML, out status);

            System.Diagnostics.Debug.WriteLine("Initial XML" + outXML);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(outXML);

            for (int index = 0; index < numberOfScanners; index++)
            {
                string scannerID = xmlDoc.DocumentElement.GetElementsByTagName("scannerID").Item(index).InnerText.Trim();
                string scannerSerial = xmlDoc.DocumentElement.GetElementsByTagName("serialnumber").Item(index).InnerText.Trim();
                //Console.WriteLine("serial:"+ scannerSerial +"--ID:" + scannerID+"--serial a buscar:"+serialNumberScanner);
                if (scannerSerial == serialNumberScanner.Trim())
                {
                    Console.WriteLine("serial:" + scannerSerial + " -- ID:" + scannerID);
                    return scannerID;
                } 
            }
            return null;
        }

        void BeepErrorScanner(string serialNumberScanner,string beep)
        {
            string[] actions = new string[] { "1", "47", "48", "47", "48" };
            string inXml = null;
            int opCode = 6000;
            string outXml = "";
            int status_beep = 1;
            string ScannerId = this.GetScannerID(serialNumberScanner);
            foreach (string action in actions)
            {
                inXml = "<inArgs>" +
                                       "<scannerID>" + ScannerId + "</scannerID>" +
                                       "<cmdArgs>" +
                                       "<arg-int>" + action
                                       + "</arg-int>" +
                                       "</cmdArgs>" +
                                       "</inArgs>";
                cCoreScannerClass.ExecCommand(opCode, ref inXml, out outXml, out status_beep);
            }
        }

    }
}
