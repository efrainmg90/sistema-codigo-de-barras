﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lector.DTO
{
    public class LogBarcodeDto
    {
        public long Barcode { get; set; }

        public string Type { get; set; }

        public long Serial { get; set; }
    }
}