﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lector
{
    class FileConfig
    {
        public void LoadConfigurationServer()
        {
            try
            {
                string pathFile = AppDomain.CurrentDomain.BaseDirectory+"Config/server.txt";
                using (StreamReader strRead = new StreamReader(pathFile))
                {
                    while (strRead.Peek()> -1)
                    {
                        string linetxt = strRead.ReadLine();
                        if (!String.IsNullOrEmpty(linetxt))
                        {
                            linetxt.Trim();
                            Console.WriteLine(linetxt);
                        }
                    }
                }
              
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
        }
        public Dictionary<string, string[]> LoadConfigurationScanners()
        {
            try
            {
                string pathFile = AppDomain.CurrentDomain.BaseDirectory + "Config/scanners.txt";
                using (StreamReader strRead = new StreamReader(pathFile))
                {
                    var dict = new Dictionary<string, string[]>();
                    while (strRead.Peek() > -1)
                    {
                        string linetxt = strRead.ReadLine();
                        if (!String.IsNullOrEmpty(linetxt))
                        {
                            linetxt.Trim();
                            string[] values = linetxt.Split('_');
                            dict.Add(values[0], values);
                            Console.WriteLine(linetxt);
                        }
                    }
                    return dict;
                }
              
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                return null;
            }
        }



    }
}
